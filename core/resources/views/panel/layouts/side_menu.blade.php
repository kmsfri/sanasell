<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">

            <ul class="sidebar-nav">

                <li class="sidebar-brand col-md-12">
                      <div><img class="img img-thumbnail img-responsive" src="{{url( '/uploads/users/admin/'.Auth::guard('admin')->user()->avatar_dir)}}" id="side-menu-avatar"></div>
                      <div>{{ Auth::guard('admin')->user()->user_title}}</div>
                      <div>{{ Auth::guard('admin')->user()->description }}</div>
                      <hr class="hr1">
                </li>

                <li><a class="master-el side-el-dash" href="{{url('admin/dashboard')}}">پیشخوان</a></li>
                <li>
                    <a class="master-el" href="javascript::void();">کاربران<img src="{{url('uploads/static/admin/arrow.png')}}"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="{{Route('userList')}}">کاربران وبسایت<span></span></a></li>
                        <li><a class="master-el" href="{{Route('adminList')}}">کاربران بخش مدیریت<span></span></a></li>
                    </ul>
                </li>
                <li>
                    <a class="master-el" href="javascript::void();">سفارشات فروش<img src="{{url('uploads/static/admin/arrow.png')}}"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="{{Route('orderList',[1,0])}}">جدید<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[1,1])}}">در دست بررسی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[1,2])}}">انجام شده<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[1,3])}}">نهایی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[1,4])}}">ناموفق<span></span></a></li>
                    </ul>
                </li>
                <li>
                    <a class="master-el" href="javascript::void();">سفارشات خرید<img src="{{url('uploads/static/admin/arrow.png')}}"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="{{Route('orderList',[0,0])}}">جدید<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[0,1])}}">در دست بررسی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[0,2])}}">انجام شده<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[0,3])}}">نهایی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('orderList',[0,4])}}">ناموفق<span></span></a></li>
                    </ul>
                </li>
                <li><a class="master-el" href="{{Route('coinList')}}">لیست ارزها</a></li>
                <li>
                    <a class="master-el" href="javascript::void();">تراکنش ها<img src="{{url('uploads/static/admin/arrow.png')}}"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="{{Route('paymentList')}}">پرداختهای ریالی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('transferList')}}">تراکنش کوین ها<span></span></a></li>
                    </ul>
                </li>
                <li>
                    <a class="master-el" href="javascript::void();">تیکتها<img src="{{url('uploads/static/admin/arrow.png')}}"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="{{Route('ticketList')}}">لیست تیکتها<span></span></a></li>
                        <li><a class="master-el" href="{{Route('AdminShowCreateGroupTicketForm')}}">ارسال تیکت گروهی<span></span></a></li>
                    </ul>
                </li>
                <li>
                    <a class="master-el" href="javascript::void();">محتوا<img src="{{url('uploads/static/admin/arrow.png')}}"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="{{Route('editInformation')}}">تنظیمات تکمیلی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('comment.index')}}">نظرات<span></span></a></li>
                        <li><a class="master-el" href="{{Route('editConfig')}}">تنظیمات کلی<span></span></a></li>
                        <li><a class="master-el" href="{{Route('editType')}}">تنظیمات نوع کاربر<span></span></a></li>
                        <li><a class="master-el" href="{{Route('adminLists')}}">لیست ها<span></span></a></li>
                        <li><a class="master-el" href="{{Route('contentsList')}}">مطالب<span></span></a></li>
                        <li><a class="master-el" href="{{Route('notesList')}}">یادداشت<span></span></a></li>
                        <li><a class="master-el" href="{{Route('updateCoinPrice')}}" target="_blank">آپدیت قیمت ارزها<span></span></a></li>
                        <li><a class="master-el" href="{{Route('selfiesBackupForm')}}">نسخه پشتیبان فایلها<span></span></a></li>
                    </ul>
                </li>


            </ul>
        </div>
        <!-- /#sidebar-wrapper -->


</div>
    <!-- /#wrapper -->

    <!-- Menu Toggle Script -->
    <script>

    $(".master-el").click(function() {
      $('.inner-ul').hide('fast' , 'swing');
      $(this).siblings('.inner-ul').slideDown('fast');
      //$(this).siblings('.inner-ul').slideToggle();
    });


    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
