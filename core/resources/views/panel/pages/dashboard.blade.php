@extends('panel.master')
@section('header')
    @include('panel.layouts.header')
@stop
@section('side-menu')
    @include('panel.layouts.side_menu')
@stop
@section('cssFiles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
@stop
@section('jsFiles')
    <script type="text/javascript" src="{!! asset('js/alert.js') !!}"></script>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@stop
@section('content')
    <div id="dashboard-cont">

        <div class="row">
            <div class="col-lg-3 pull-right">
                <div class="row">
                    <div class="pull-right dash-row1-sec1">{{$adminUsersCount}}</div>
                    <div class="pull-right dash-row1-sec2">کاربران ادمین</div>
                </div>
            </div>
            <div class="col-lg-3 pull-right">
                <div class="row">
                    <div class="pull-right dash-row1-sec1">{{$authedUsersCount}}</div>
                    <div class="pull-right dash-row1-sec2">کاربران احراز شده</div>
                </div>
            </div>
            <div class="col-lg-3 pull-right">
                <div class="row">
                    <div class="pull-right dash-row1-sec1">{{$contentsCount}}</div>
                    <div class="pull-right dash-row1-sec2">مطالب</div>
                </div>
            </div>
            <div class="col-lg-3 pull-right">
                <div class="row">
                    <div class="pull-right dash-row1-sec1">{{$dollarPrice}}</div>
                    <div class="pull-right dash-row1-sec2">قیمت دلار(تومان)</div>
                </div>
            </div>
        </div>

        <div class="row dash-row">
            <div class="col-lg-6 pull-right out-col">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="row dash-row2-right-sec1 dash-paid-amount"></br>{{$sumOfPayments}} تومان</div>
                        <div class="row dash-row2-right-sec2">کل واریزی</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row dash-row2-right-sec1">{{$coinsCount}}</div>
                        <div class="row dash-row2-right-sec2">کل ارزها</div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 pull-right out-col">
                <div class="row">

                    <div class="col-lg-6 pull-right dash-row2-left-el">
                        <div class="row">
                            <div class="pull-right dash-row2-left-sec1">{{$totalOrdersCount}}</div>
                            <div class="pull-right dash-row2-left-sec2">کل سفارشات</div>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-right dash-row2-left-el">
                        <div class="row">
                            <div class="pull-right dash-row2-left-sec1">{{$newOrdersCount}}</div>
                            <div class="pull-right dash-row2-left-sec2">سفارشات جدید</div>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-right dash-row2-left-el">
                        <div class="row">
                            <div class="pull-right dash-row2-left-sec1">{{$endedOrdersCount}}</div>
                            <div class="pull-right dash-row2-left-sec2">سفارشات نهایی شده</div>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-right dash-row2-left-el">
                        <div class="row">
                            <div class="pull-right dash-row2-left-sec1">{{$doneOrdersCount}}</div>
                            <div class="pull-right dash-row2-left-sec2">سفارشات تایید شده</div>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-right dash-row2-left-el">
                        <div class="row">
                            <div class="pull-right dash-row2-left-sec1">{{$transfersCount}}</div>
                            <div class="pull-right dash-row2-left-sec2">تعداد تراکنشها</div>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-right dash-row2-left-el">
                        <div class="row">
                            <div class="pull-right dash-row2-left-sec1">{{$unseedTicketMessages}}</div>
                            <div class="pull-right dash-row2-left-sec2">پیام جدید تیکتها</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="row dash-row">
            <div class="col-lg-3 pull-right">
                <div class="row dash-row3-right-sec1"></div>
                <div class="row dash-row3-right-sec2">
                    <span class="row3-title1">بازدید امروز: </span><span class="row3-title2">0 بازدید</span>
                </div>
                <div class="row dash-row3-right-sec2">
                    <span class="row3-title1">بازدید هفته: </span><span class="row3-title2">0 بازدید</span>
                </div>
                <div class="row dash-row3-right-sec2">
                    <span class="row3-title1">بازدید ماه: </span><span class="row3-title2">0 بازدید</span>
                </div>
            </div>
            <div class="col-lg-9 pull-right out-col">
                <div class="row">
                    <div class="col-lg-6">
                        <textarea id="noteTextArea" class="form-control" placeholder="یادداشت"></textarea>
                    </div>
                    <div class="col-lg-6">
                        <div class="row dash-row3-right-sec2">
                            <h3 class="text-center">جدید</h3>
                        </div>
                        <div class="row dash-row3-right-sec2">
                        <span class="row3-title1">
                            <a href='{{Route('userList')}}?sortType=unseen'>
                            درخواست احراز هویت :
                            </a>
                            </span><span class="row3-title2" id="unseen_user_profile_updates_count_cont">0</span>
                        </div>
                        <div class="row dash-row3-right-sec2">
                        <span class="row3-title1">
                            <a href='{{Route('ticketList')}}?sortType=unseen'>
                            پیام جدید(تیکت):
                            </a>
                        </span><span class="row3-title2" id="unseen_ticket_messages_count_cont">0</span>
                        </div>
                        <div class="row dash-row3-right-sec2">
                        <span class="row3-title1">
                            <a href='{{Route('orderList',[0,0])}}?sortType=unseen'>
                            سفارش خرید جدید:
                            </a>
                        </span><span class="row3-title2"id="unseen_buy_orders_count_cont">0</span>

                        </div>
                        <div class="row dash-row3-right-sec2">
                        <span class="row3-title1">
                            <a href='{{Route('orderList',[1,0])}}?sortType=unseen'>
                            سفارش فروش جدید:
                            </a>
                        </span><span class="row3-title2"id="unseen_sell_orders_count_cont">0</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 pull-right">
                        <div class="hour-cont" id="time-cont-up">
                            {{ $nowTime }}
                        </div>
                    </div>
                    <div class="col-lg-4 pull-right">
                        <div class="date-cont">
                            {{ $nowDate }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function showUnseenRecordsCount() {
            $.ajax({
                url: "{{Route('getUnseenRecordsCount')}}",
                data: {
                    "_token":"{{csrf_token()}}"
                },
                type: 'POST',

                success: function(data){
                    console.log(data);
                    if(data.result=="ok"){

                        $('#unseen_sell_orders_count_cont').html(data.unseen_sell_orders);
                        if(data.unseen_sell_orders>0){
                            $( "#unseen_sell_orders_count_cont" ).effect( "shake" );
                        }

                        $('#unseen_buy_orders_count_cont').html(data.unseen_buy_orders);
                        if(data.unseen_buy_orders>0){
                            $( "#unseen_buy_orders_count_cont" ).effect( "shake" );
                        }



                        $('#unseen_ticket_messages_count_cont').html(data.unseen_ticket_messages);
                        if(data.unseen_ticket_messages>0){
                            $( "#unseen_ticket_messages_count_cont" ).effect( "shake" );
                        }


                        $('#unseen_user_profile_updates_count_cont').html(data.unseen_user_profile_updates);
                        if(data.unseen_user_profile_updates>0){
                            $( "#unseen_user_profile_updates_count_cont" ).effect( "shake" );
                        }


                        if(data.unseen_buy_orders>0 || data.unseen_sell_orders>0 || data.unseen_ticket_messages>0 || data.unseen_user_profile_updates>0){
                            playAlert('purr')
                        }


                    }else{
                        $('#unseen_sell_orders_count_cont').html('خطا در اتصال');
                        $('#unseen_buy_orders_count_cont').html('خطا در اتصال');
                        $('#unseen_ticket_messages_count_cont').html('خطا در اتصال');
                        $('#unseen_user_profile_updates_count_cont').html('خطا در اتصال');

                    }
                },
                error: function(data){
                    $('#unseen_sell_orders_count_cont').html('خطا در اتصال');
                    $('#unseen_buy_orders_count_cont').html('خطا در اتصال');
                    $('#unseen_ticket_messages_count_cont').html('خطا در اتصال');
                    $('#unseen_user_profile_updates_count_cont').html('خطا در اتصال');
                }
            });
        }




        $(document).ready (function (){
            showUnseenRecordsCount();
            $(function () {
                setInterval(showUnseenRecordsCount, 60000);
            });
            startCount();
        });

        function startCount(){
            timer = setInterval(count,1000);
        }
        function count(){
            var time_shown = $("#time-cont-up").text();
            var time_chunks = time_shown.split(":");
            var hour, mins, secs;
            hour=Number(time_chunks[0]);
            mins=Number(time_chunks[1]);
            secs=Number(time_chunks[2]);

            secs++;
            if (secs==60){
                secs = 0;
                mins=mins + 1;
            }
            if (mins==60){
                mins=0;
                hour=hour + 1;
            }
            if (hour==13){
                hour=1;
            }
            $("#time-cont-up").text(hour +":" + plz(mins) + ":" + plz(secs));
        }

        function plz(digit){
            var zpad = digit + '';
            if (digit < 10) {
                zpad = "0" + zpad;
            }
            return zpad;
        }
    </script>


@stop
