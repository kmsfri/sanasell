@extends('admin.master')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content')
<div class="panel-heading-cs1">
    <div class="pull-left">
        <a href="{{ $backward_url }}" class="btn btn-back-cs1">بازگشت</a>
    </div>
    <div class="pull-left">
        @if($add_url!=Null)
            <a href="{{$add_url}}" class="btn btn-btn1-cs1">افزودن</a>
        @endif
    </div>
    <div class="pull-left">
        <form id="delForm" action="{{$delete_url}}" method="post" onsubmit="return confirm('آیا از انجام اینکار مطمئنید؟');">
            {{ csrf_field() }}
            <input type="hidden" name="opType" id="opType" value="Delete">
            <button type="submit" class="btn btn-btn1-cs1" onClick="$('#opType').val('Delete')">حذف</button>
        </form>
    </div>
    <div class="panel-heading-cs1-title">{!! $title  !!}</div>
</div>
<div class="panel-body panel-body-cs1">
    <div class="table-responsive table_posts_container">
        <div id="selectableCont" class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                @yield('selectable_elements')
                </div>
            </div>
        </div>
        <table class="table table-striped table-hover">
            @yield('content_list')
        </table>
        <div class="row">
            @yield('paginationCont')
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    @yield('extraButtons')
                </div>
            </div>
            </br></br>
        </div>
    </div>
</div>
@stop