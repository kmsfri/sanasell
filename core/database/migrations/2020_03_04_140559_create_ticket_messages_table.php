<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ticket_id')->nullable();
            $table->unsignedTinyInteger('sender')->comment('0:operator - 1:customer');
            $table->string('message_text',1500);
            $table->string('file_dir')->nullable();
            $table->timestamp('seen_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('ticket_id')->references('id')->on('tickets')->onUpdate('cascade')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_messages');
    }
}
