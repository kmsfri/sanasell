<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('operator_user_id')->nullable();
            $table->bigInteger('customer_user_id')->nullable();
            $table->string('ticket_title');
            $table->unsignedTinyInteger('ticket_status')->default(0);
            $table->unsignedTinyInteger('ticket_order')->default(1)->comment('1:urgent - 2:high - 3:medium - 4:low');
            $table->unsignedTinyInteger('create_type')->default(1)->comment('1:single - 2:bulk');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('operator_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('customer_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
