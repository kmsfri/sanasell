<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//})->middleware('auth')->middleware('check_user_role:' . \App\Role\UserRole::ROLE_ADMIN);;

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'GeneralController@dashboard')->name('dashboard')->middleware('check_user_role:' . \App\Role\UserRole::ROLE_ADMIN);
Route::resource('users', 'UserController')->middleware('check_user_role:' . \App\Role\UserRole::ROLE_RESELLER);
