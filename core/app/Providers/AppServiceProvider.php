<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $this->app->singleton(\App\Http\Middleware\CheckUserRole::class, function(Application $app) {
//            return new CheckUserRole(
//                $app->make(RoleChecker::class)
//            );
//        });
    }
}
