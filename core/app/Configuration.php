<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table = 'configuration';
    protected $primaryKey = 'id';


    public function DollarPrice(){
        return $this->where('config_key','=','DollarPrice');
    }
    
    public function WageAmount(){
        return $this->where('config_key','=','WageAmount');
    }
    
    public function WebsiteClosed(){
        return $this->where('config_key','=','WebsiteClosed');
    }
}
