<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;

class CheckUserRole
{
    protected $roleChecker;
    public function __construct(\App\Role\RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        $user = Auth::guard()->user();

        if ( ! $this->roleChecker->check($user, $role)) {
            throw new AuthorizationException('You do not have permission to view this page');
        }

        return $next($request);
    }
}
