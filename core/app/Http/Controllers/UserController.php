<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use App\Role\UserRole;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from users service API
        $client = new \GuzzleHttp\Client();
        $request = $client->post('http://localhost:8001/graphql/', [
            'form_params' => [
                'query' => ''
            ]
        ]);
        $response = $request->getBody()->getContents();



        //$shows = ;
        return view('users.index', compact('shows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email|max:100|unique:users,email',
            'password' => 'required|max:20',
            'user_level' => 'required|integer'
        ]);

        //send data to users service API endpoint
        $client = new \GuzzleHttp\Client();
        $request = $client->post('http://localhost:8001/graphql/', [
            'form_params' => [
                'query' => ''
            ]
        ]);
        $response = $request->getBody()->getContents();

        return redirect('/users')->with('success', 'کاربر جدید با موفقیت اضافه شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //get data from users service API
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get data from users service API
        $client = new \GuzzleHttp\Client();
        $request = $client->post('http://localhost:8001/graphql/', [
            'form_params' => [
                'query' => ''
            ]
        ]);
        $response = $request->getBody()->getContents();
        //$show =;

        return view('users.create', compact('show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email|max:100|unique:users,email,'.$id,
            'password' => 'nullable|max:20',
            'user_level' => 'required|integer'
        ]);
        if(!empty($request->password)){
            $validatedData['password'] = bcrypt($request->password);
        }else{
            unset($validatedData['password']);
        }


        //send data to users service API endpoint
        $client = new \GuzzleHttp\Client();
        $request = $client->post('http://localhost:8001/graphql/', [
            'form_params' => [
                'query' => ''
            ]
        ]);
        $response = $request->getBody()->getContents();

        return redirect('/users')->with('success', 'کاربر مورد نظر با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //send data to users service API endpoint
        $client = new \GuzzleHttp\Client();
        $request = $client->post('http://localhost:8001/graphql/', [
            'form_params' => [
                'query' => ''
            ]
        ]);
        $response = $request->getBody()->getContents();

        return redirect('/users')->with('success', 'کاربر مورد نظر با موفقیت حذف شد');
    }
}
