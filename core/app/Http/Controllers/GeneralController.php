<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use App\Role\UserRole;
use App\User;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function showDashboard(Request $request){

        $now_datetime= \Carbon\Carbon::now()->toDateTimeString();
        $now_datetime_arr=explode(' ',$now_datetime);

        $dashData['nowTime']=$now_datetime_arr[1];

        $t_date=\Helpers::convert_date_g_to_j($now_datetime_arr[0],true);
        $t_date=explode('/',$t_date);
        $month=\Helpers::get_equal_str_month($t_date[1]);
        $dashData['nowDate']=$t_date[2].' '.$month.' '.$t_date[0];

        $dashData['adminUsersCount']=User::hasRole(UserRole::ROLE_ADMIN)->count();

        $dashData['authedUsersCount']=User::where('identity_status',1)->count();

        $dashData['dollarPrice']=Configuration::first()->DollarPrice()->first()->values;

        $dashData['sumOfPayments']=0;

        $dashData['totalOrdersCount']=0;

        $dashData['newOrdersCount']=0;

        $dashData['endedOrdersCount']=0;

        $dashData['doneOrdersCount']=0;



        return view('panel.pages.dashboard' ,$dashData);

    }
}
