<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $client = new \GuzzleHttp\Client();

        $response = $client->post('http://localhost:8001/graphql/', [
            'form_params' => [
                'query' => '
                {
                  user(name: "developer",id: 1) {
                    name,email
                  }
                }'
            ]
        ]);

        echo $response->getBody()->getContents();
        die();


        return true;
        $user = Auth::user();

        $user->addRole('ROLE_ADMIN');
        $user->save();
        dd($user);
        return view('home');
    }
}
