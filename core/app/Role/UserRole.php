<?php

namespace App\Role;

/***
 * Class UserRole
 * @package App\Role
 */
class UserRole {

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_RESELLER = 'ROLE_RESELLER';
    const ROLE_OPERATOR = 'ROLE_OPERATOR';
    const ROLE_CUSTOMER = 'ROLE_CUSTOMER';
    const ROLE_SUPPORT = 'ROLE_SUPPORT';

    /**
     * @var array
     */
    protected static $roleHierarchy = [
        self::ROLE_ADMIN => ['*'],
        self::ROLE_RESELLER => [
            self::ROLE_OPERATOR,
            self::ROLE_CUSTOMER,
            self::ROLE_SUPPORT
        ],
        self::ROLE_OPERATOR => [
            self::ROLE_RESELLER,
            self::ROLE_CUSTOMER,
            self::ROLE_SUPPORT,
        ],
        self::ROLE_CUSTOMER => [],
        self::ROLE_SUPPORT => []
    ];

    /**
     * @param string $role
     * @return array
     */
    public static function getAllowedRoles(string $role)
    {
        if (isset(self::$roleHierarchy[$role])) {
            return self::$roleHierarchy[$role];
        }

        return [];
    }

    /***
     * @return array
     */
    public static function getRoleList()
    {
        return [
            static::ROLE_ADMIN =>'Admin',
            static::ROLE_OPERATOR => 'Operator',
            static::ROLE_RESELLER => 'Reseller',
            static::ROLE_CUSTOMER => 'Customer',
            static::ROLE_SUPPORT => 'Support',
        ];
    }

}
