<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $primaryKey = 'id';

    public function User()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function CoinOrder()
    {
        return $this->belongsTo('App\Models\CoinOrder','order_id');
    }



    public function TicketMessages()
    {
        return $this->hasMany('App\Models\TicketMessage','ticket_id');
    }

    public function UnseenTicketMessages()
    {
        return $this->hasMany('App\Models\TicketMessage','ticket_id')->where('admin_seen',0);
    }
}
