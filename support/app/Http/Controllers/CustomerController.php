<?php

namespace App\Http\Controllers\Landing\panel;

use App\Models\Ticket;
use \App\Models\TicketMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use DB;

class CustomerController extends Controller
{
    public function createNewTicket(Request $request){
        $data=[
            'orderID'=>$request->orderID,
        ];
        $validator = Validator::make(
            $data,
            [
                'orderID'=>'nullable|integer|exists:user_coin_order,id',
            ]
        );


        if($validator->fails()){
            $msg=[
                0=>[
                    'msgType'=>'danger',
                    'msgText'=>'ورودیها نامعتبر هستند',
                ]
            ];
            return back()->with('messages', $msg);
        }


        $user=Auth::guard('user')->user();


        $order=Null;

        if(isset($request->orderID)) {
            $order = $user->Orders()
                ->where('id', $request->orderID)
                ->with('Coin')
                ->first();

            if ($order == Null) {
                $msg = [
                    0 => [
                        'msgType' => 'danger',
                        'msgText' => 'ورودیها نامعتبر هستند',
                    ]
                ];
                return back()->with('messages', $msg);
            }

        }

        $orders = $user->Orders()
            ->with('Coin')
            ->orderBy('created_at', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->get();

        foreach ($orders as $o){
            $title='';
            if($o->order_type==0){
                $title='خرید ';
            }else{
                $title='فروش ';
            }
            $title.=$o->coin_amount.' '.$o->Coin->coin_name.' به قیمت '.number_format($o->total_price).' تومان در تاریخ '.\Helpers::convert_date_g_to_j($o->created_at,true).' با شناسه '.$o->id;
            $o->title=$title;
        }


        $data=[
            'order_id'=>($order!=Null)?$order->id:Null,
            'orders'=>$orders,
        ];
        return view('landing.pages.add_ticket',$data);

    }




    public function doCreateNewTicket(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'order'=>'nullable|integer|exists:user_coin_order,id',
                'ticket_title'=>'required|max:100',
                'ticket_order'=>'required|integer',
                'message_text'=>'required|max:1500'
            ]
        );


        if($validator->fails()){
            $msg=[
                0=>[
                    'msgType'=>'danger',
                    'msgText'=>'ورودیها نامعتبر هستند',
                ]
            ];
            return back()->with('messages', $msg);
        }


        $user=Auth::guard('user')->user();

        $ticket=new Ticket();
        $ticket->order_id=$request->order;
        $ticket->user_id=$user->id;
        $ticket->ticket_title=$request->ticket_title;
        $ticket->ticket_order=$request->ticket_order;
        $ticket->ticket_status=0;
        $ticket->save();

        $ticketMessage=new TicketMessage();
        $ticketMessage->sender=1;
        $ticketMessage->message_text=nl2br($request->message_text);
        $ticketMessage->user_seen=1;

        $ticket->TicketMessages()->save($ticketMessage);

        $ticket->touch();




        //begin notify admins
        $user=Auth::guard('user')->user();
        $title="ارسال تیکت جدید";
        $messageText="کاربر:".$user->email.' یک تیکت جدید ایجاد کرده است.';
        \Helpers::notifyAdmins($title,$messageText,['user'=>$user->email],3219);
        //end notify admins




        $msg=[
            0=>[
                'msgType'=>'success',
                'msgText'=>'تیکت جدید با موفقیت ارسال شد',
            ]

        ];

        return redirect(Route('showTicketList'))
            ->with('messages',$msg);
    }








    public function showTicketList(Request $request){
        $user = Auth::guard('user')->user();

        $tickets=$user;
        if(isset($request->orderID)){
            $tickets = $tickets->Orders()->where('id',$request->orderID)->first();
            if($tickets==Null) abort(404);
            $title="لیست تیکت های مربوط به سفارش شماره: ".$request->orderID;
        }else{
            $title="لیست تیکت ها";
        }
        $tickets = $tickets->Tickets()
            ->withCount(['TicketMessages'=> function ($query){
                $query->where('user_seen',0);
            }])
            ->orderBy('tickets.updated_at','DESC')
            ->orderBy('tickets.created_at','DESC')
            ->get();



        $data=[
            'tickets'=>$tickets,
            'title'=>$title,
            'order_id'=>isset($request->orderID)?$request->orderID:Null,
        ];
        return view('landing.pages.ticketList',$data);
    }




    public function getTicketDetails(Request $request){
        $toValidateData=[
            'ticket_id'=>$request->ticket_id,
        ];
        $validator=Validator::make($toValidateData,[
            'ticket_id'=>'required|exists:tickets,id'
        ]);

        if ($validator->fails()) {
            $msg=[
                0=>[
                    'msgType'=>'danger',
                    'msgText'=>'ورودیها نامعتبر هستند',
                ]
            ];
            return back()->with('messages', $msg);
        }

        $user = Auth::guard('user')->user();

        $ticket=$user->Tickets()->findOrFail($request->ticket_id);

        $ticketMessages=array();
        if($ticket!=Null){
            $ticketMessages=$ticket->TicketMessages()->get();
        }


        $tmIDs=$ticket->TicketMessages()->get()->pluck('id')->toArray();

        DB::table('ticket_messages')
            ->whereIn('id',$tmIDs)
            ->update(['user_seen'=>1]);

        $data=[
            'ticket' => $ticket,
            'ticketMessages' => $ticketMessages,
            'backward_url'=>url()->previous(),
        ];
        return view('landing.pages.ticketDetails',$data);
    }


    var $file_upload_error_msg='';
    var $destinationPath = '/uploads/users/tickets';
    public function saveNewMessage(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'ticket_id' => 'required|integer|exists:tickets,id',
                'message_text'=>'required|max:1500',
                'file_dir' => 'nullable|mimes:png,jpg,jpeg,pnf|max:2048',
            ]
        );


        if($validator->fails()){
            $msg=[
                0=>[
                    'msgType'=>'danger',
                    'msgText'=>'ورودیها نامعتبر هستند',
                ]
            ];
            return back()->with('messages', $msg);
        }


        $user = Auth::guard('user')->user();


        $ticket = Ticket::where('id', $request->ticket_id)
            ->where('user_id', $user->id)
            ->where('ticket_status', 0)
            ->first();

        if($ticket==Null){
            abort(404);
        }

        $uploaded_file_dir=Null;
        if($request->file_dir!=Null){
            $this->img_upload_error_msg=array();

            if($request->file_dir!=Null) {
                $file = $request->file('file_dir');
                $fileName = "";
                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = $user->id.time() . '_' . $file->getClientOriginalName();

                        $file->move(public_path().$this->destinationPath, $fileName);
                        $uploaded_file_dir = $this->destinationPath.'/'.$fileName;
                    } else {
                        $msg=[
                            0=>[
                                'msgType'=>'danger',
                                'msgText'=>'آپلود فایل ناموفق بود',
                            ]

                        ];
                        return back()->with('messages',$msg);
                    }
                }

            }
        }


        $ticketMessage=new TicketMessage();
        $ticketMessage->sender=1;
        $ticketMessage->message_text=nl2br($request->message_text);
        $ticketMessage->file_dir=$uploaded_file_dir;
        $ticketMessage->user_seen=1;

        $ticket->TicketMessages()->save($ticketMessage);

        $ticket->touch();


        //begin notify admins
        $user=Auth::guard('user')->user();
        $title="ارسال پیام جدید";
        $messageText="کاربر:".$user->email.'یک پیام جدید ارسال کرده است.';
        \Helpers::notifyAdmins($title,$messageText,['user'=>$user->email],3220);
        //end notify admins


        $msg=[
            0=>[
                'msgType'=>'success',
                'msgText'=>'پیام جدید با موفقیت ارسال شد',
            ]

        ];

        return redirect(Route('getTicketDetails',$ticket->id))
            ->with('messages',$msg);
    }





}
