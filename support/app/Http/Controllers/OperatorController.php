<?php

namespace App\Http\Controllers\Admin;

use App\Models\CoinOrder;
use App\Models\Ticket;
use App\Models\TicketMessage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Auth;
use Helpers;

class OperatorController extends Controller
{
    public function showTicketList(Request $request){
        $add_url=Null;
        if(isset($request->user_id)){
            $user=User::findOrFail($request->user_id);
            $tickets = $user->Tickets();
            $title='تیکت های کاربر: '.$user->mobile_number;
        }else if(isset($request->order_id)){
            $order=CoinOrder::findOrFail($request->order_id);
            $tickets = $order->Tickets();
            $title='تیکت های سفارش: '.$order->id;
        }else{

            $title="تیکت ها";



            $tickets=Ticket::select('*');

            if(isset($request->search_key) && $request->search_key!=Null){
                $tickets=$tickets->where(function($query) use($request) {
                    $query->orWhere('id', '=' , $request->search_key)
                        ->orWhere('ticket_title', 'like', '%'.$request->search_key.'%');
                });
            }


            if(isset($request->sortType) && $request->sortType!=Null){
                if($request->sortType=='unseen'){

                    $tickets=$tickets->withCount('UnseenTicketMessages')
                        ->orderBy('unseen_ticket_messages_count', 'desc');


                }else if($request->sortType=='newest'){
                    $tickets=$tickets->orderBy('created_at','DESC');
                }else if($request->sortType=='older'){
                    $tickets=$tickets->orderBy('created_at','ASC');
                }else if($request->sortType=='updated'){
                    $tickets=$tickets->orderBy('updated_at','DESC');
                }
            }


        }
        $tickets=$tickets
            ->withCount(['TicketMessages'=> function ($query){
                $query->where('admin_seen',0);
            }])
            ->orderBy('tickets.updated_at','DESC')->orderBy('tickets.created_at','DESC')->paginate(20);

        $data=[
            'title'=>$title,
            'add_url'=>$add_url,
            'tickets'=>$tickets->appends(\Illuminate\Support\Facades\Input::except('page')),
            'backward_url'=>url()->previous(),
            'delete_url'=>Null,
            'sortType'=>(isset($request->sortType))?$request->sortType:Null,
            'search_key'=>(isset($request->search_key))?$request->search_key:Null,
        ];
        return view('admin.pages.lists.tickets',$data);
    }




    public function showTicketMessages(Request $request){

        $toValidateData=[
            'ticket_id'=>$request->ticket_id,
        ];
        $validator=Validator::make($toValidateData,[
            'ticket_id'=>'required|exists:tickets,id'
        ]);

        if ($validator->fails()) {

            $msg=["ورودیها نامعتبر هستند"];
            return back()->with('messages', $msg);
        }


        $ticket=Ticket::findOrFail($request->ticket_id);

        $ticketMessages=array();
        if($ticket!=Null){
            $ticketMessages=$ticket->TicketMessages()->get();
        }


        $tmIDs=$ticket->TicketMessages()->get()->pluck('id')->toArray();

        DB::table('ticket_messages')
            ->whereIn('id',$tmIDs)
            ->update(['admin_seen'=>1]);

        $data=[
            'request_type'=>'edit',
            'title'=>'لیست پیام های تیکت: '.$ticket->ticket_title,
            'ticket' => $ticket,
            'ticketMessages' => $ticketMessages,
            'backward_url'=>url()->previous(),
            'post_edit_url'=>Null,
            'edit_id'=>$ticket->id,
        ];
        return view('admin.pages.forms.ticketDetails',$data);
    }


    /*
        public function doSaveTicket(Request $request){

            $validator = Validator::make(
                $request->all(),
                [
                    'ticket_id' => 'nullable|integer|exists:shoper_buyer_tickets,id',
                    'user_id' => 'nullable|integer|exists:users,id',
                    'order_id' => 'nullable|integer|exists:user_coin_orders,id',
                    'message_text'=>'nullable|max:280',
                    'ticket_status' => 'required|integer',
                ]
            );

            if($validator->fails()){
                return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors($validator->errors())
                    ->with('data','ورودی های خود را بررسی کنید');
            }

            if(isset($request->ticket_id) && $request->ticket_id!=Null){
                $ticket=Ticket::where('id',$request->ticket_id)->first();
            }else{
                $ticket=new Ticket();
                $ticket->user_id=$request->user_id;
                $ticket->order_id=$request->order_id;
            }
            $ticket->ticket_status=$request->ticket_status;
            $ticket->save();

            if($request->message_text!=Null) {
                $ticketMessage = new TicketMessage();
                $ticketMessage->sender = 0;
                $ticketMessage->message_text = nl2br($request->message_text);

                $ticket->TicketMessages()->save($ticketMessage);
                $ticket->touch();
            }


            if(isset($request->ticket_id) && $request->ticket_id!=Null){
                $msg=["پیام مورد نظر با موفقیت بروزرسانی شد"];

                $title="پیام جدید";
                $messageText='کاربر گرامی، یک پیام جدید برای شما در وبسایت پی ارز ارسال شده است. لطفا به پنل کاربری، بخش تیکتها مراجعه کنید';
            }else{
                $msg=["تیکت جدید با موفقیت ایجاد شد"];

                $title="تیکت جدید";
                $messageText='کاربر گرامی، یک تیکت جدید برای شما در وبسایت پی ارز ارسال شده است. لطفا به پنل کاربری، بخش تیکتها مراجعه کنید';
            }
            \Helpers::notifyUser($ticket->User->email,$ticket->User->mobile_number,$title,$messageText,['time'=>Helpers::convert_date_g_to_j($ticket->created_at,true)],3232);


            return redirect(url(Route('ticketMessages',$ticket->id)))->with('messages', $msg);

        }
    */





    var $file_upload_error_msg='';
    var $destinationPath = '/uploads/users/tickets';
    public function doAddTicketMessage(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'ticket_id' => 'nullable|integer|exists:tickets,id',
                'message_text'=>'required|max:1500',
                'file_dir' => 'nullable|mimes:png,jpg,jpeg,pnf|max:2048',
            ]
        );


        if($validator->fails()){
            $msg=['ورودیها نامعتبر هستند'];
            return back()->with('messages', $msg);
        }


        $ticket = Ticket::where('id', $request->ticket_id)
            ->where('ticket_status', 0)
            ->first();

        if($ticket==Null){
            abort(404);
        }

        $uploaded_file_dir=Null;
        if($request->file_dir!=Null){
            $this->img_upload_error_msg=array();

            if($request->file_dir!=Null) {
                $file = $request->file('file_dir');
                $fileName = "";
                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = Auth::guard('admin')->user()->id.time() . '_' . $file->getClientOriginalName();

                        $file->move(public_path().$this->destinationPath, $fileName);
                        $uploaded_file_dir = $this->destinationPath.'/'.$fileName;
                    } else {
                        $msg=['آپلود فایل ناموفق بود'];
                        return back()->with('messages',$msg);
                    }
                }

            }
        }


        $ticketMessage=new TicketMessage();
        $ticketMessage->admin_user_id=Auth::guard('admin')->user()->id;
        $ticketMessage->sender=0;
        $ticketMessage->message_text=nl2br($request->message_text);
        $ticketMessage->file_dir=$uploaded_file_dir;
        $ticketMessage->admin_seen=1;

        $ticket->TicketMessages()->save($ticketMessage);

        $ticket->touch();

        $title="پیام جدید";
        $messageText='کاربر گرامی، یک پیام جدید برای شما در وبسایت پی ارز ارسال شده است. لطفا به پنل کاربری، بخش تیکتها مراجعه کنید';
        \Helpers::notifyUser($ticket->User->email,$ticket->User->mobile_number,$title,$messageText,['time'=>Helpers::convert_date_g_to_j($ticket->created_at,true)],3233);


        $msg=['پیام جدید با موفقیت ارسال شد'];

        return back()->with('messages',$msg);
    }






    public function doCreateTicket(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|integer|exists:users,id',
                'ticket_title'=>'required|max:255',
                'ticket_order'=>'required|integer|between:1,4',
                'order'=>'nullable|integer|exists:user_coin_order,id',
                'message_text'=>'required|max:1500',
            ]
        );




        if($validator->fails()){
            $msg=['ورودیها نامعتبر هستند'];
            return back()->with('messages', $msg);
        }

        $user=\App\Models\User::findOrFail($request->user_id);

        $ticket=new Ticket();
        $ticket->order_id=$request->order;
        $ticket->user_id=$user->id;
        $ticket->ticket_title=$request->ticket_title;
        $ticket->ticket_order=$request->ticket_order;
        $ticket->ticket_status=0;
        $ticket->save();

        $ticketMessage=new TicketMessage();
        $ticketMessage->sender=0;
        $ticketMessage->message_text=nl2br($request->message_text);
        $ticketMessage->admin_seen=1;

        $ticket->TicketMessages()->save($ticketMessage);

        $ticket->touch();

        $title="پیام جدید";
        $messageText='کاربر گرامی، یک تیکت جدید برای شما در وبسایت پی ارز از طرف مدیریت وبسایت ایجاد شده است. برای مشاهده‌ی جزئیات لطفا به پنل کاربری خود مراجعه فرمایید.';
        \Helpers::notifyUser($ticket->User->email,$ticket->User->mobile_number,$title,$messageText,['time'=>Helpers::convert_date_g_to_j($ticket->created_at,true)],3232);


        $msg=['تیکت جدید با موفقیت ارسال شد'];

        return Redirect(Route('ticketMessages',$ticket->id))->with('messages',$msg);
    }






    public function showCreateTicketForm(Request $request){
        $data=[
            'user_id'=>$request->user_id,
        ];
        $validator = Validator::make(
            $data,
            [
                'user_id'=>'required|integer|exists:users,id',
            ]
        );


        if($validator->fails()){
            $msg=['ورودیها نامعتبر هستند'];
            return back()->with('messages', $msg);
        }


        $user=\App\Models\User::findOrFail($request->user_id);

        $orders = $user->Orders()
            ->with('Coin')
            ->orderBy('created_at', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->get();


        foreach ($orders as $o){
            $title='';
            if($o->order_type==0){
                $title='خرید ';
            }else{
                $title='فروش ';
            }
            $title.=$o->coin_amount.' '.$o->Coin->coin_name.' به قیمت '.number_format($o->total_price).' تومان در تاریخ '.\Helpers::convert_date_g_to_j($o->created_at,true).' با شناسه '.$o->id;
            $o->title=$title;
        }


        $data=[
            'orders'=>$orders,
            'user_id'=>$request->user_id,
            'request_type'=>'add',
            'title'=>'ارسال تیکت جدید به: '.$user->email,
            'backward_url'=>url()->previous(),
            'post_add_url'=>Route('doCreateTicket'),
        ];
        return view('admin.pages.forms.addTicket',$data);


    }







    public function showCreateGroupTicketForm(Request $request){


        $data=[
            'request_type'=>'add',
            'title'=>'ارسال تیکت گروهی',
            'backward_url'=>url()->previous(),
            'post_add_url'=>Route('doCreateGroupTicket'),
        ];
        return view('admin.pages.forms.addGroupTicket',$data);


    }





    public function doCreateGroupTicket(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'group_type'=>'required|integer|between:1,3',
                'ticket_title'=>'required|max:255',
                'ticket_order'=>'required|integer|between:1,4',
                'message_text'=>'required|max:1500',
            ]
        );

        if($validator->fails()){
            $msg=['ورودیها نامعتبر هستند'];
            return back()->with('messages', $msg);
        }


        if($request->group_type==1){
            $users=\App\Models\User::select('id','mobile_number','first_name','email')
                ->get();
        }elseif($request->group_type==2){
            $users=\App\Models\User::select('id','mobile_number','first_name','email')
                ->where('auth_status',2)->get();
        }elseif($request->group_type==3){
            $users=\App\Models\User::select('id','mobile_number','first_name','email')
                ->where('auth_status','!=',2)->get();
        }

        foreach($users as $user){
            $ticket=new Ticket();
            $ticket->user_id=$user->id;
            $ticket->ticket_title=$request->ticket_title;
            $ticket->ticket_order=$request->ticket_order;
            $ticket->ticket_status=0;
            $ticket->create_type=2; //bulk
            $ticket->save();

            $ticketMessage=new TicketMessage();
            $ticketMessage->sender=0;
            $ticketMessage->message_text=nl2br($request->message_text);
            $ticketMessage->admin_seen=1;

            $ticket->TicketMessages()->save($ticketMessage);

            $ticket->touch();

            $title="پیام جدید";
            $messageText='کاربر گرامی، یک تیکت جدید برای شما در وبسایت پی ارز از طرف مدیریت وبسایت ایجاد شده است. برای مشاهده‌ی جزئیات لطفا به پنل کاربری خود مراجعه فرمایید.';
            \Helpers::notifyUser($user->email,Null,$title,$messageText,['time'=>Helpers::convert_date_g_to_j($ticket->created_at,true)],Null);
        }


        $msg=['تیکت های جدید با موفقیت ارسال شدند'];

        return Redirect(Route('ticketList'))->with('messages',$msg);
    }









}
